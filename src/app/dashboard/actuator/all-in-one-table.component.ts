import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/takeUntil';
import {Observable} from 'rxjs/Observable';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {routeAnimation} from '../../core/common/route.animation';
import {ListColumn} from '../../core/list/list-column.model';
import {ListDatabase} from '../../core/list/list-database';
import {ListDataSource} from '../../core/list/list-datasource';
import {List} from '../../core/list/list.interface';
import {ALL_IN_ONE_TABLE_DEMO_DATA} from './all-in-one-table.demo';
import {ActuatorCreateUpdateComponent} from './actuator-create-update/actuator-create-update.component';
import {Actuator} from './actuator-create-update/actuator.model';

@Component({
  selector: 'vr-all-in-one-table',
  templateUrl: './all-in-one-table.component.html',
  styleUrls: ['./all-in-one-table.component.scss'],
  animations: [routeAnimation],
  host: { '[@routeAnimation]': 'true' }
})
export class AllInOneTableComponent implements List<Actuator>, OnInit, OnDestroy {

  subject$: ReplaySubject<Actuator[]> = new ReplaySubject<Actuator[]>(1);
  data$: Observable<Actuator[]>;
  actuators: Actuator[];

  @Input()
  columns: ListColumn[] = [
    { name: 'Name', property: 'name', visible: true, isModelProperty: true },
    { name: 'IP Address', property: 'firstName', visible: true, isModelProperty: true },
    { name: 'MAC', property: 'city', visible: false, isModelProperty: true },
    { name: 'Place Info', property: 'lastName', visible: true, isModelProperty: true },
    { name: 'Command', property: 'street', visible: false, isModelProperty: true },
    { name: 'Status', property: 'zipcode', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ] as ListColumn[];
  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<Actuator> | null;
  database: ListDatabase<Actuator>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.actuators = ALL_IN_ONE_TABLE_DEMO_DATA.map(actuator => new Actuator(actuator));

    this.subject$.next(this.actuators);
    this.data$ = this.subject$.asObservable();

    this.database = new ListDatabase<Actuator>();
    this.data$
      .filter(Boolean)
      .subscribe((actuators) => {
        this.actuators = actuators;
        this.database.dataChange.next(actuators);
        this.resultsLength = actuators.length;
      });

    this.dataSource = new ListDataSource<Actuator>(this.database, this.sort, this.paginator, this.columns);
  }

  createActuator() {
    this.dialog.open(ActuatorCreateUpdateComponent).afterClosed().subscribe((actuator: Actuator) => {
      if (actuator) {
        this.actuators.unshift(new Actuator(actuator));
        this.subject$.next(this.actuators);
      }
    });
  }

  updateActuator(actuator) {
    this.dialog.open(ActuatorCreateUpdateComponent, {
      data: actuator
    }).afterClosed().subscribe((actuator) => {
      if (actuator) {
        const index = this.actuators.findIndex((existingActuator) => existingActuator.id === actuator.id);
        this.actuators[index] = new Actuator(actuator);
        this.subject$.next(this.actuators);
      }
    });
  }

  deleteActuator(actuator) {
    this.actuators.splice(this.actuators.findIndex((existingActuator) => existingActuator.id === actuator.id), 1);
    this.subject$.next(this.actuators);
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }
}
