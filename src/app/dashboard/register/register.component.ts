﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService, UserService } from '../../_services/index';
import { routeAnimation} from '../../core/common/route.animation';

@Component({
    moduleId: module.id,
    selector: 'vr-form-elements',
    templateUrl: 'register.component.html',
    styleUrls: ['./register.component.scss'],
    host: {
        '[@routeAnimation]': 'true'
    },
    animations: [ routeAnimation ]
})
export class RegisterComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { }

    register() {
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
