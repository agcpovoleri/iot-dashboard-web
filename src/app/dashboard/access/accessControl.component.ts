import {Component, OnInit, ViewChild} from '@angular/core';
import {routeAnimation} from '../../core/common/route.animation';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import { AccessControlService } from '../../_services/index';

@Component({
  selector: 'vr-form-elements',
  templateUrl: './accessControl.component.html',
  styleUrls: ['./accessControl.component.scss'],
  host: {
    '[@routeAnimation]': 'true'
  },
  animations: [ routeAnimation ]
})
export class AccessControlComponent implements OnInit {

  rows: any = {};

  tableHover: boolean = true;
  tableStriped: boolean = true;
  tableCondensed: boolean = true;
  tableBordered: boolean = true;

  pageSize = 10;
  resultsLength: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private accessControlService: AccessControlService) { }

    ngOnInit() {

        this.accessControlService.getAll().subscribe(
            data => {
                //this.alertService.success(data.login);
                this.rows = data
            });
    }
}
