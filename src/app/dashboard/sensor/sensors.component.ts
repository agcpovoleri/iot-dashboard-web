import {Component, OnInit, ViewChild} from '@angular/core';
import {routeAnimation} from '../../core/common/route.animation';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import { SensorService } from '../../_services/index';

@Component({
  selector: 'vr-form-elements',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.scss'],
  host: {
    '[@routeAnimation]': 'true'
  },
  animations: [ routeAnimation ]
})
export class SensorsComponent implements OnInit {

  rows: any = {};

  tableHover: boolean = true;
  tableStriped: boolean = true;
  tableCondensed: boolean = true;
  tableBordered: boolean = true;

  pageSize = 10;
  resultsLength: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private sensorService: SensorService) { }

    ngOnInit() {

        this.sensorService.getAll().subscribe(
            data => {
                //this.alertService.success(data.login);
                this.rows = data
            });
    }
}
