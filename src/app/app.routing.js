"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
// import { HomeComponent } from './dashboard/home/index';
// import { LoginComponent } from './dashboard/login/index';
// import { RegisterComponent } from './dashboard/register/index';
// import { AuthGuard } from './_guards/index';
var core_1 = require("@angular/core");
var admin_component_1 = require("./core/admin/admin.component");
var forgot_password_component_1 = require("./demo/custom-pages/forgot-password/forgot-password.component");
var login_component_1 = require("./demo/custom-pages/login/login.component");
var register_component_1 = require("./demo/custom-pages/register/register.component");
var dashboard_component_1 = require("./demo/dashboard/dashboard.component");
var drag_and_drop_component_1 = require("./demo/drag-and-drop/drag-and-drop.component");
var editor_component_1 = require("./demo/editor/editor.component");
var form_elements_component_1 = require("./demo/forms/form-elements/form-elements.component");
var form_wizard_component_1 = require("./demo/forms/form-wizard/form-wizard.component");
var icons_component_1 = require("./demo/icons/icons.component");
var level5_component_1 = require("./demo/levels/level5/level5.component");
var google_maps_component_1 = require("./demo/maps/google-maps/google-maps.component");
var routes = [
    {
        path: 'login',
        component: login_component_1.LoginComponent
    },
    {
        path: 'register',
        component: register_component_1.RegisterComponent
    },
    {
        path: 'forgot-password',
        component: forgot_password_component_1.ForgotPasswordComponent
    },
    {
        path: '',
        component: admin_component_1.AdminComponent,
        children: [
            {
                path: '',
                component: dashboard_component_1.DashboardComponent,
                pathMatch: 'full'
            },
            {
                path: 'apps/inbox',
                loadChildren: 'app/demo/apps/inbox/inbox.module#InboxModule',
            },
            {
                path: 'apps/calendar',
                loadChildren: 'app/demo/apps/calendar/calendar.module#CalendarAppModule',
            },
            {
                path: 'apps/chat',
                loadChildren: 'app/demo/apps/chat/chat.module#ChatModule',
            },
            {
                path: 'components',
                loadChildren: 'app/demo/components/components.module#ComponentsModule',
            },
            {
                path: 'forms/form-elements',
                component: form_elements_component_1.FormElementsComponent
            },
            {
                path: 'forms/form-wizard',
                component: form_wizard_component_1.FormWizardComponent
            },
            {
                path: 'icons',
                component: icons_component_1.IconsComponent
            },
            {
                path: 'level1/level2/level3/level4/level5',
                component: level5_component_1.Level5Component
            },
            {
                path: 'maps/google-maps',
                component: google_maps_component_1.GoogleMapsComponent
            },
            {
                path: 'tables/simple-table',
                loadChildren: 'app/demo/tables/simple-table/simple-table.module#SimpleTableModule',
            },
            {
                path: 'tables/all-in-one-table',
                loadChildren: 'app/demo/tables/all-in-one-table/all-in-one-table.module#AllInOneTableModule',
            },
            {
                path: 'drag-and-drop',
                component: drag_and_drop_component_1.DragAndDropComponent
            },
            {
                path: 'editor',
                component: editor_component_1.EditorComponent
            },
        ]
    }
];
var RoutingModule = /** @class */ (function () {
    function RoutingModule() {
    }
    RoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], RoutingModule);
    return RoutingModule;
}());
exports.RoutingModule = RoutingModule;
//# sourceMappingURL=app.routing.js.map