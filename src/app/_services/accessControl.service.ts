﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SensorData } from '../_models/index';
import {AccessData} from "../_models/accessData";

@Injectable()
export class AccessControlService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<AccessData[]>('/api/access');
    }
}
