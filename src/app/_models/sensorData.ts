﻿export class SensorData {
    id: number;
    sourceUid: string;
    category: string;
    content: string;
    createTimestamp: string;
}
