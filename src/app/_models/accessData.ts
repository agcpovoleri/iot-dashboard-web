﻿export class AccessData {
    id: number;
    sourceUid: string;
    category: string;
    content: string;
    createTimestamp: string;
}
