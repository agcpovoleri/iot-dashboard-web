"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var animations_1 = require("@angular/animations");
/*
Deprecated until Opacity works again

export let routeAnimation = trigger('routeAnimation', [
  transition('void => *', [
    style({
      transform: 'translate3d(0, 10%, 0)',
      opacity: 0,
    }),
    group([
      animate('400ms ease-in-out', style({
        //transform: 'translate3d(0, 0, 0)',
        transform: 'translate3d(0, 0, 0)',
        opacity: 1,
      })),
      animate('400ms 150ms ease-in-out', style({
        opacity: 1,
      }))
    ]),
  ]),
]);
*/
exports.routeAnimation = animations_1.trigger('routeAnimation', [
    animations_1.transition('void => *', [
        animations_1.style({
            opacity: 0,
        }),
        animations_1.animate('400ms 150ms ease-in-out', animations_1.style({
            opacity: 1,
        }))
    ]),
]);
exports.fadeInAnimation = animations_1.trigger('fadeInAnimation', [
    animations_1.transition('void => *', [
        animations_1.style({
            opacity: 0,
        }),
        animations_1.animate('400ms 150ms ease-in-out', animations_1.style({
            opacity: 1,
        }))
    ]),
]);
//# sourceMappingURL=route.animation.js.map