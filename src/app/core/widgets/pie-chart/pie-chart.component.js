"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nvD3_service_1 = require("../../charts/nvD3/nvD3.service");
var PieChartComponent = /** @class */ (function () {
    function PieChartComponent(d3ChartService) {
        this.isLoading = false;
        this.d3 = d3ChartService.getD3();
    }
    PieChartComponent.prototype.ngOnInit = function () {
        var d3 = this.d3;
        if (!this.chartOptions) {
            this.chartOptions = {
                chart: {
                    type: 'pieChart',
                    height: 400,
                    margin: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0
                    },
                    x: function (d) { return d.label; },
                    y: function (d) { return d.value; },
                    showXAxis: false,
                    showYAxis: false,
                    showLegend: true,
                    useInteractiveGuideline: true,
                    donut: true,
                    color: ['#1E88E5', '#D81B60', '#00897B', '#3949AB', '#e53935']
                },
            };
        }
    };
    PieChartComponent.prototype.reload = function () {
        var _this = this;
        this.isLoading = true;
        setTimeout(function () {
            _this.isLoading = false;
        }, 3000);
    };
    __decorate([
        core_1.Input('data'),
        __metadata("design:type", Object)
    ], PieChartComponent.prototype, "data", void 0);
    __decorate([
        core_1.Input('chartOptions'),
        __metadata("design:type", Object)
    ], PieChartComponent.prototype, "chartOptions", void 0);
    __decorate([
        core_1.Input('widgetTitle'),
        __metadata("design:type", String)
    ], PieChartComponent.prototype, "widgetTitle", void 0);
    __decorate([
        core_1.Input('subTitle'),
        __metadata("design:type", String)
    ], PieChartComponent.prototype, "subTitle", void 0);
    __decorate([
        core_1.Input('bgColor'),
        __metadata("design:type", String)
    ], PieChartComponent.prototype, "bgColor", void 0);
    __decorate([
        core_1.Input('textColor'),
        __metadata("design:type", String)
    ], PieChartComponent.prototype, "textColor", void 0);
    PieChartComponent = __decorate([
        core_1.Component({
            selector: 'vr-pie-chart',
            templateUrl: './pie-chart.component.html'
        }),
        __param(0, core_1.Inject(nvD3_service_1.D3ChartService)),
        __metadata("design:paramtypes", [nvD3_service_1.D3ChartService])
    ], PieChartComponent);
    return PieChartComponent;
}());
exports.PieChartComponent = PieChartComponent;
//# sourceMappingURL=pie-chart.component.js.map