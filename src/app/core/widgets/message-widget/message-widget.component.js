"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var MessageWidgetComponent = /** @class */ (function () {
    function MessageWidgetComponent() {
    }
    MessageWidgetComponent.prototype.ngOnInit = function () {
        this.messages = [
            {
                image: 'assets/img/avatars/1.png',
                from: 'Jonah Muisel',
                message: 'Hey! What\'s up?',
                online: true
            },
            {
                image: 'assets/img/avatars/2.png',
                from: 'Conner Oreilly',
                message: 'Love your newest theme!',
                online: true
            },
            {
                image: 'assets/img/avatars/3.png',
                from: 'Zac Husein',
                message: 'Don\'t forget to update!',
                online: false
            },
            {
                image: 'assets/img/avatars/4.png',
                from: 'Jenniffer Litt',
                message: 'Last weekend was great xoxo',
                online: false
            }
        ];
    };
    MessageWidgetComponent = __decorate([
        core_1.Component({
            selector: 'vr-message-widget',
            templateUrl: './message-widget.component.html',
            styleUrls: ['./message-widget.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], MessageWidgetComponent);
    return MessageWidgetComponent;
}());
exports.MessageWidgetComponent = MessageWidgetComponent;
//# sourceMappingURL=message-widget.component.js.map