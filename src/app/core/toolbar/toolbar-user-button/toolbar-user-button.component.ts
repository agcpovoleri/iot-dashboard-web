import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../_services/index';

@Component({
  selector: 'vr-toolbar-user-button',
  templateUrl: './toolbar-user-button.component.html',
  styleUrls: ['./toolbar-user-button.component.scss']
})
export class ToolbarUserButtonComponent implements OnInit {

  isOpen: boolean;
  loggedUser: any;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
      this.loggedUser  = JSON.parse(this.authenticationService.getAuthenticatedUser());
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  onClickOutside() {
    this.isOpen = false;
  }

}
