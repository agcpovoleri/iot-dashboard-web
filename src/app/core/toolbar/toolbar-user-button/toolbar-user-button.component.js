"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ToolbarUserButtonComponent = /** @class */ (function () {
    function ToolbarUserButtonComponent() {
    }
    ToolbarUserButtonComponent.prototype.ngOnInit = function () {
    };
    ToolbarUserButtonComponent.prototype.toggleDropdown = function () {
        this.isOpen = !this.isOpen;
    };
    ToolbarUserButtonComponent.prototype.onClickOutside = function () {
        this.isOpen = false;
    };
    ToolbarUserButtonComponent = __decorate([
        core_1.Component({
            selector: 'vr-toolbar-user-button',
            templateUrl: './toolbar-user-button.component.html',
            styleUrls: ['./toolbar-user-button.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], ToolbarUserButtonComponent);
    return ToolbarUserButtonComponent;
}());
exports.ToolbarUserButtonComponent = ToolbarUserButtonComponent;
//# sourceMappingURL=toolbar-user-button.component.js.map