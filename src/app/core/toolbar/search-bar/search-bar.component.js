"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var sidenav_service_1 = require("../../sidenav/sidenav.service");
var SearchBarComponent = /** @class */ (function () {
    function SearchBarComponent(router, sidenavService) {
        this.router = router;
        this.sidenavService = sidenavService;
        this.recentlyVisited = [];
    }
    SearchBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setupDemoData();
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                var item = _this.sidenavService.getSidenavItemByRoute(event.urlAfterRedirects);
                if (item) {
                    var index = _this.recentlyVisited.indexOf(item);
                    if (index > -1) {
                        _this.recentlyVisited.splice(index, 1);
                    }
                    _this.recentlyVisited.unshift(item);
                    if (_this.recentlyVisited.length > 5) {
                        _this.recentlyVisited.pop();
                    }
                }
            }
        });
    };
    SearchBarComponent.prototype.setupDemoData = function () {
        var formWizard = this.sidenavService.getSidenavItemByRoute('/forms/form-wizard');
        if (formWizard)
            this.recentlyVisited.push(formWizard);
        var inbox = this.sidenavService.getSidenavItemByRoute('/apps/inbox');
        if (inbox)
            this.recentlyVisited.push(inbox);
        var allInOneTable = this.sidenavService.getSidenavItemByRoute('/tables/all-in-one-table');
        if (allInOneTable)
            this.recentlyVisited.push(allInOneTable);
        var editor = this.sidenavService.getSidenavItemByRoute('/editor');
        if (editor)
            this.recentlyVisited.push(editor);
        var googleMaps = this.sidenavService.getSidenavItemByRoute('/maps/google-maps');
        if (googleMaps)
            this.recentlyVisited.push(googleMaps);
    };
    SearchBarComponent.prototype.openDropdown = function () {
        this.focused = true;
    };
    SearchBarComponent.prototype.closeDropdown = function () {
        this.focused = false;
    };
    SearchBarComponent = __decorate([
        core_1.Component({
            selector: 'vr-search-bar',
            templateUrl: './search-bar.component.html',
            styleUrls: ['./search-bar.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router,
            sidenav_service_1.SidenavService])
    ], SearchBarComponent);
    return SearchBarComponent;
}());
exports.SearchBarComponent = SearchBarComponent;
//# sourceMappingURL=search-bar.component.js.map