"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var pending_interceptor_service_1 = require("./pending-interceptor.service");
var PendingInterceptorServiceExistingProvider = {
    provide: http_1.HTTP_INTERCEPTORS,
    useExisting: pending_interceptor_service_1.PendingInterceptorService,
    multi: true
};
var PendingInterceptorModule = /** @class */ (function () {
    function PendingInterceptorModule() {
    }
    PendingInterceptorModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                http_1.HttpClientModule
            ],
            declarations: [],
            providers: [
                PendingInterceptorServiceExistingProvider,
                pending_interceptor_service_1.PendingInterceptorServiceFactoryProvider
            ],
        })
    ], PendingInterceptorModule);
    return PendingInterceptorModule;
}());
exports.PendingInterceptorModule = PendingInterceptorModule;
//# sourceMappingURL=pending-interceptor.module.js.map