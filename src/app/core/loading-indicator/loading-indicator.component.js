"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var timer_1 = require("rxjs/observable/timer");
var operators_1 = require("rxjs/operators");
var pending_interceptor_service_1 = require("./pending-interceptor.service");
var LoadingIndicatorComponent = /** @class */ (function () {
    function LoadingIndicatorComponent(pendingRequestInterceptorService) {
        var _this = this;
        this.pendingRequestInterceptorService = pendingRequestInterceptorService;
        this.filteredUrlPatterns = [];
        this.debounceDelay = 100;
        this.entryComponent = null;
        this.subscription = this.pendingRequestInterceptorService
            .pendingRequestsStatus
            .pipe(operators_1.debounce(this.handleDebounce.bind(this)))
            .subscribe(function (hasPendingRequests) { return _this.isSpinnerVisible = hasPendingRequests; });
    }
    LoadingIndicatorComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!(this.filteredUrlPatterns instanceof Array)) {
            throw new TypeError('`filteredUrlPatterns` must be an array.');
        }
        if (!!this.filteredUrlPatterns.length) {
            this.filteredUrlPatterns.forEach(function (e) {
                _this.pendingRequestInterceptorService.filteredUrlPatterns.push(new RegExp(e));
            });
        }
    };
    LoadingIndicatorComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    LoadingIndicatorComponent.prototype.handleDebounce = function (hasPendingRequests) {
        if (hasPendingRequests) {
            return timer_1.timer(this.debounceDelay);
        }
        return timer_1.timer(0);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LoadingIndicatorComponent.prototype, "backgroundColor", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], LoadingIndicatorComponent.prototype, "filteredUrlPatterns", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], LoadingIndicatorComponent.prototype, "debounceDelay", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], LoadingIndicatorComponent.prototype, "entryComponent", void 0);
    LoadingIndicatorComponent = __decorate([
        core_1.Component({
            selector: 'vr-loading-indicator',
            templateUrl: './loading-indicator.component.html',
            styleUrls: ['./loading-indicator.component.scss']
        }),
        __metadata("design:paramtypes", [pending_interceptor_service_1.PendingInterceptorService])
    ], LoadingIndicatorComponent);
    return LoadingIndicatorComponent;
}());
exports.LoadingIndicatorComponent = LoadingIndicatorComponent;
//# sourceMappingURL=loading-indicator.component.js.map