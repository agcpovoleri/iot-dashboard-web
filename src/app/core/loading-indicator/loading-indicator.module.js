"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var loading_indicator_component_1 = require("./loading-indicator.component");
var LoadingIndicatorModule = /** @class */ (function () {
    function LoadingIndicatorModule() {
    }
    LoadingIndicatorModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                http_1.HttpClientModule,
                material_1.MatProgressBarModule
            ],
            declarations: [loading_indicator_component_1.LoadingIndicatorComponent],
            exports: [loading_indicator_component_1.LoadingIndicatorComponent],
        })
    ], LoadingIndicatorModule);
    return LoadingIndicatorModule;
}());
exports.LoadingIndicatorModule = LoadingIndicatorModule;
//# sourceMappingURL=loading-indicator.module.js.map