"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
require("rxjs/add/observable/throw");
var Observable_1 = require("rxjs/Observable");
var operators_1 = require("rxjs/operators");
var ReplaySubject_1 = require("rxjs/ReplaySubject");
var PendingInterceptorService = /** @class */ (function () {
    function PendingInterceptorService(router) {
        var _this = this;
        this._pendingRequests = 0;
        this._pendingRequestsStatus = new ReplaySubject_1.ReplaySubject(1);
        this._filteredUrlPatterns = [];
        router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                _this._pendingRequestsStatus.next(true);
            }
            if ((event instanceof router_1.NavigationError || event instanceof router_1.NavigationEnd || event instanceof router_1.NavigationCancel)) {
                _this._pendingRequestsStatus.next(false);
            }
        });
    }
    Object.defineProperty(PendingInterceptorService.prototype, "pendingRequests", {
        get: function () {
            return this._pendingRequests;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PendingInterceptorService.prototype, "pendingRequestsStatus", {
        get: function () {
            return this._pendingRequestsStatus.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PendingInterceptorService.prototype, "filteredUrlPatterns", {
        get: function () {
            return this._filteredUrlPatterns;
        },
        enumerable: true,
        configurable: true
    });
    PendingInterceptorService.prototype.intercept = function (req, next) {
        var _this = this;
        var shouldBypass = this.shouldBypass(req.url);
        if (!shouldBypass) {
            this._pendingRequests++;
            if (1 === this._pendingRequests) {
                this._pendingRequestsStatus.next(true);
            }
        }
        return next.handle(req).pipe(operators_1.map(function (event) {
            return event;
        }), operators_1.catchError(function (error) {
            return Observable_1.Observable.throw(error);
        }), operators_1.finalize(function () {
            if (!shouldBypass) {
                _this._pendingRequests--;
                if (0 === _this._pendingRequests) {
                    _this._pendingRequestsStatus.next(false);
                }
            }
        }));
    };
    PendingInterceptorService.prototype.shouldBypass = function (url) {
        return this._filteredUrlPatterns.some(function (e) {
            return e.test(url);
        });
    };
    PendingInterceptorService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], PendingInterceptorService);
    return PendingInterceptorService;
}());
exports.PendingInterceptorService = PendingInterceptorService;
function PendingInterceptorServiceFactory(router) {
    return new PendingInterceptorService(router);
}
exports.PendingInterceptorServiceFactory = PendingInterceptorServiceFactory;
exports.PendingInterceptorServiceFactoryProvider = {
    provide: PendingInterceptorService,
    useFactory: PendingInterceptorServiceFactory,
    deps: [router_1.Router]
};
//# sourceMappingURL=pending-interceptor.service.js.map