"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var sidenav_item_model_1 = require("./sidenav-item.model");
var sidenav_service_1 = require("../sidenav.service");
var SidenavItemComponent = /** @class */ (function () {
    function SidenavItemComponent(sidenavService) {
        this.sidenavService = sidenavService;
        this.sidenavItemClass = true;
    }
    Object.defineProperty(SidenavItemComponent.prototype, "isOpen", {
        get: function () {
            return this.sidenavService.isOpen(this.item);
        },
        enumerable: true,
        configurable: true
    });
    SidenavItemComponent.prototype.ngOnInit = function () {
    };
    SidenavItemComponent.prototype.toggleDropdown = function () {
        if (this.item.hasSubItems()) {
            this.sidenavService.toggleCurrentlyOpen(this.item);
        }
    };
    // Receives the count of Sub Items and multiplies it with 48 (height of one SidenavItem) to set the height for animation.
    SidenavItemComponent.prototype.getSubItemsHeight = function () {
        return (this.getOpenSubItemsCount(this.item) * 48) + "px";
    };
    // Counts the amount of Sub Items there is and returns the count.
    SidenavItemComponent.prototype.getOpenSubItemsCount = function (item) {
        var _this = this;
        var count = 0;
        if (item.hasSubItems() && this.sidenavService.isOpen(item)) {
            count += item.subItems.length;
            item.subItems.forEach(function (subItem) {
                count += _this.getOpenSubItemsCount(subItem);
            });
        }
        return count;
    };
    __decorate([
        core_2.Input('item'),
        __metadata("design:type", sidenav_item_model_1.SidenavItem)
    ], SidenavItemComponent.prototype, "item", void 0);
    __decorate([
        core_1.HostBinding('class.open'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], SidenavItemComponent.prototype, "isOpen", null);
    __decorate([
        core_1.HostBinding('class.sidenav-item'),
        __metadata("design:type", Boolean)
    ], SidenavItemComponent.prototype, "sidenavItemClass", void 0);
    SidenavItemComponent = __decorate([
        core_1.Component({
            selector: 'vr-sidenav-item',
            templateUrl: './sidenav-item.component.html',
            styleUrls: ['./sidenav-item.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [sidenav_service_1.SidenavService])
    ], SidenavItemComponent);
    return SidenavItemComponent;
}());
exports.SidenavItemComponent = SidenavItemComponent;
//# sourceMappingURL=sidenav-item.component.js.map