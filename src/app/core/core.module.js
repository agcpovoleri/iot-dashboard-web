"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var material_1 = require("@angular/material");
var router_1 = require("@angular/router");
var admin_component_1 = require("./admin/admin.component");
var breadcrumb_component_1 = require("./breadcrumb/breadcrumb.component");
var breadcrumb_service_1 = require("./breadcrumb/breadcrumb.service");
var click_outside_directive_1 = require("./common/click-outside.directive");
var material_components_module_1 = require("./common/material-components.module");
var loading_indicator_module_1 = require("./loading-indicator/loading-indicator.module");
var pending_interceptor_module_1 = require("./loading-indicator/pending-interceptor.module");
var quickpanel_component_1 = require("./quickpanel/quickpanel.component");
var scrollbar_module_1 = require("./scrollbar/scrollbar.module");
var icon_sidenav_directive_1 = require("./sidenav/icon-sidenav.directive");
var media_replay_service_1 = require("./sidenav/mediareplay/media-replay.service");
var sidenav_item_component_1 = require("./sidenav/sidenav-item/sidenav-item.component");
var sidenav_component_1 = require("./sidenav/sidenav.component");
var sidenav_service_1 = require("./sidenav/sidenav.service");
var search_bar_component_1 = require("./toolbar/search-bar/search-bar.component");
var search_component_1 = require("./toolbar/search/search.component");
var toolbar_notifications_component_1 = require("./toolbar/toolbar-notifications/toolbar-notifications.component");
var toolbar_user_button_component_1 = require("./toolbar/toolbar-user-button/toolbar-user-button.component");
var toolbar_component_1 = require("./toolbar/toolbar.component");
var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                http_1.HttpClientModule,
                router_1.RouterModule,
                material_components_module_1.MaterialModule,
                forms_1.FormsModule,
                scrollbar_module_1.ScrollbarModule,
                loading_indicator_module_1.LoadingIndicatorModule,
                pending_interceptor_module_1.PendingInterceptorModule,
                forms_1.ReactiveFormsModule
            ],
            declarations: [
                sidenav_component_1.SidenavComponent,
                sidenav_item_component_1.SidenavItemComponent,
                icon_sidenav_directive_1.IconSidenavDirective,
                search_component_1.SearchComponent,
                breadcrumb_component_1.BreadcrumbsComponent,
                admin_component_1.AdminComponent,
                quickpanel_component_1.QuickpanelComponent,
                toolbar_component_1.ToolbarComponent,
                toolbar_user_button_component_1.ToolbarUserButtonComponent,
                click_outside_directive_1.ClickOutsideDirective,
                search_bar_component_1.SearchBarComponent,
                toolbar_notifications_component_1.ToolbarNotificationsComponent
            ],
            providers: [
                material_1.MatIconRegistry,
                media_replay_service_1.MediaReplayService,
                sidenav_service_1.SidenavService,
                breadcrumb_service_1.BreadcrumbService
            ],
        })
    ], CoreModule);
    return CoreModule;
}());
exports.CoreModule = CoreModule;
//# sourceMappingURL=core.module.js.map