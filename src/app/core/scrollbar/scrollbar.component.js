"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var smooth_scrollbar_1 = require("smooth-scrollbar");
exports.scrollbarOptions = {
    speed: 1,
    damping: 0.2,
    thumbMinSize: 20,
    syncCallbacks: true,
    renderByPixels: true,
    alwaysShowTracks: false,
    continuousScrolling: true,
    overscrollEffect: 'bounce',
    overscrollDamping: 0.2
};
var ScrollbarComponent = /** @class */ (function () {
    function ScrollbarComponent(_element, zone) {
        this._element = _element;
        this.zone = zone;
    }
    ScrollbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.element = this._element;
        var options = exports.scrollbarOptions;
        this.zone.runOutsideAngular(function () {
            _this.scrollbarRef = smooth_scrollbar_1.default.init(_this.element.nativeElement, options);
        });
    };
    ScrollbarComponent = __decorate([
        core_1.Component({
            selector: 'vr-scrollbar',
            templateUrl: './scrollbar.component.html',
            styleUrls: ['./scrollbar.component.scss']
        }),
        __metadata("design:paramtypes", [core_1.ElementRef,
            core_1.NgZone])
    ], ScrollbarComponent);
    return ScrollbarComponent;
}());
exports.ScrollbarComponent = ScrollbarComponent;
//# sourceMappingURL=scrollbar.component.js.map