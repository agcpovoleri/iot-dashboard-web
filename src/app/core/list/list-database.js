"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
/** An example database that the data source uses to retrieve data for the table. */
var ListDatabase = /** @class */ (function () {
    function ListDatabase() {
        /** Stream that emits whenever the data has been modified. */
        this.dataChange = new BehaviorSubject_1.BehaviorSubject([]);
    }
    Object.defineProperty(ListDatabase.prototype, "data", {
        get: function () {
            return this.dataChange.value;
        },
        enumerable: true,
        configurable: true
    });
    return ListDatabase;
}());
exports.ListDatabase = ListDatabase;
//# sourceMappingURL=list-database.js.map