"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var collections_1 = require("@angular/cdk/collections");
require("rxjs/add/observable/merge");
require("rxjs/add/operator/map");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var Observable_1 = require("rxjs/Observable");
/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
var ListDataSource = /** @class */ (function (_super) {
    __extends(ListDataSource, _super);
    function ListDataSource(_listDatabase, _sort, _paginator, _columns) {
        var _this = _super.call(this) || this;
        _this._listDatabase = _listDatabase;
        _this._sort = _sort;
        _this._paginator = _paginator;
        _this._columns = _columns;
        _this._filterChange = new BehaviorSubject_1.BehaviorSubject('');
        _this._properties = _this._columns.filter(function (column) { return column.isModelProperty; }).map(function (column) { return column.property; });
        return _this;
    }
    Object.defineProperty(ListDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    ListDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this._listDatabase.dataChange,
            this._sort.sortChange,
            this._filterChange,
            this._paginator.page
        ];
        return Observable_1.Observable.merge.apply(Observable_1.Observable, displayDataChanges).map(function () {
            return _this.getPaginatedData(_this.getSortedData(_this.filterData()));
        });
    };
    ListDataSource.prototype.disconnect = function () {
    };
    /** Returns a sorted copy of the database data. */
    ListDataSource.prototype.getSortedData = function (filteredData) {
        var _this = this;
        var data = filteredData;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            var sortByProperty = _this._properties[_this._sort.active];
            _a = [a[sortByProperty], b[sortByProperty]], propertyA = _a[0], propertyB = _a[1];
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a;
        });
    };
    ListDataSource.prototype.filterData = function () {
        var _this = this;
        return this._listDatabase.data.slice().filter(function (model) {
            var searchStr = _this._properties.map(function (property) { return model[property]; }).join(' ').toLowerCase();
            return searchStr.indexOf(_this.filter.toLowerCase()) !== -1;
        });
    };
    ListDataSource.prototype.getPaginatedData = function (data) {
        var startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        return data.splice(startIndex, this._paginator.pageSize);
    };
    return ListDataSource;
}(collections_1.DataSource));
exports.ListDataSource = ListDataSource;
//# sourceMappingURL=list-datasource.js.map