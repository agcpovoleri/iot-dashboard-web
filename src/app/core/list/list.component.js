"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/observable/fromEvent");
require("rxjs/add/operator/debounceTime");
require("rxjs/add/operator/distinctUntilChanged");
var Observable_1 = require("rxjs/Observable");
var ListComponent = /** @class */ (function () {
    function ListComponent() {
        this.filterChange = new core_1.EventEmitter();
    }
    ListComponent.prototype.ngOnInit = function () {
        var _this = this;
        Observable_1.Observable.fromEvent(this.filter.nativeElement, 'keyup')
            .distinctUntilChanged()
            .debounceTime(150)
            .subscribe(function () {
            _this.filterChange.emit(_this.filter.nativeElement.value);
        });
    };
    ListComponent.prototype.toggleColumnVisibility = function (column, event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        column.visible = !column.visible;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ListComponent.prototype, "name", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], ListComponent.prototype, "columns", void 0);
    __decorate([
        core_1.ViewChild('filter'),
        __metadata("design:type", core_1.ElementRef)
    ], ListComponent.prototype, "filter", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ListComponent.prototype, "filterChange", void 0);
    ListComponent = __decorate([
        core_1.Component({
            selector: 'vr-list',
            templateUrl: './list.component.html',
            styleUrls: ['./list.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [])
    ], ListComponent);
    return ListComponent;
}());
exports.ListComponent = ListComponent;
//# sourceMappingURL=list.component.js.map