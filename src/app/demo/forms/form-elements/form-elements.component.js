"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var FormElementsComponent = /** @class */ (function () {
    function FormElementsComponent() {
        this.switch1 = false;
        this.switch2 = true;
        this.switch3 = true;
        this.switch4 = false;
        this.switch5 = true;
        this.checkbox1 = false;
        this.checkbox2 = true;
        this.checkbox3 = true;
        this.checkbox4 = false;
        this.checkbox5 = true;
        this.inputFieldHTML = "\n  <div fxLayout=\"column\" fxLayout.gt-md=\"row\">\n    <mat-input-container fxFlex>\n      <input matInput\n             placeholder=\"First Name\">\n    </mat-input-container>\n    <mat-input-container fxFlex>\n      <input matInput\n             placeholder=\"Last Name\">\n    </mat-input-container>\n  </div>\n  <div fxLayout=\"column\" fxLayout.gt-md=\"row\">\n    <mat-input-container fxFlex dividerColor=\"accent\">\n      <input matInput\n             placeholder=\"Pre-filled Input + Accent color\" value=\"Material Design\">\n    </mat-input-container>\n  </div>\n  <div fxLayout=\"column\" fxLayout.gt-md=\"row\">\n    <mat-input-container fxFlex>\n      <input matInput\n             placeholder=\"Address\">\n      <span mat-prefix><mat-icon>place</mat-icon></span>\n    </mat-input-container>\n    <mat-input-container fxFlex>\n      <input matInput\n             placeholder=\"City\">\n      <span mat-prefix><mat-icon>location_city</mat-icon></span>\n    </mat-input-container>\n    <mat-input-container fxFlex>\n      <input matInput\n             placeholder=\"Country\">\n      <span mat-suffix><mat-icon>local_airport</mat-icon></span>\n    </mat-input-container>\n  </div>\n  <div fxLayout=\"column\" fxLayout.gt-md=\"row\">\n    <mat-input-container fxFlex>\n      <input matInput\n             placeholder=\"Character count (max. 100)\"\n             maxlength=\"100\"\n             #characterCountHintExample\n             value=\"Hello! How are you today?\">\n      <mat-hint align=\"end\">{{ characterCountHintExample.value.length }} / 100</mat-hint>\n    </mat-input-container>\n  </div>\n  <div fxLayout=\"column\" fxLayout.gt-md=\"row\">\n    <mat-input-container fxFlex>\n      <input matInput\n             placeholder=\"Primary Color\">\n    </mat-input-container>\n    <mat-input-container dividerColor=\"accent\" fxFlex>\n      <input matInput\n             placeholder=\"Accent Color\">\n    </mat-input-container>\n    <mat-input-container dividerColor=\"warn\" fxFlex>\n      <input matInput\n             placeholder=\"Warn Color\">\n    </mat-input-container>\n  </div>\n  ";
    }
    FormElementsComponent.prototype.ngOnInit = function () {
    };
    FormElementsComponent.prototype.getEscaped = function (text) {
        return escape_1.default(text);
    };
    FormElementsComponent = __decorate([
        core_1.Component({
            selector: 'vr-form-elements',
            templateUrl: './form-elements.component.html',
            styleUrls: ['./form-elements.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [route_animation_1.routeAnimation]
        }),
        __metadata("design:paramtypes", [])
    ], FormElementsComponent);
    return FormElementsComponent;
}());
exports.FormElementsComponent = FormElementsComponent;
//# sourceMappingURL=form-elements.component.js.map