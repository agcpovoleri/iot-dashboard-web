"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@agm/core");
var common_1 = require("@angular/common");
var http_1 = require("@angular/common/http");
var core_2 = require("@angular/core");
var flex_layout_1 = require("@angular/flex-layout");
var forms_1 = require("@angular/forms");
var angular_sortablejs_1 = require("angular-sortablejs");
var ngx_quill_1 = require("ngx-quill");
var environment_1 = require("../../environments/environment");
var app_routing_1 = require("../../app/app.routing");
var nvD3_component_1 = require("../core/charts/nvD3/nvD3.component");
var nvD3_service_1 = require("../core/charts/nvD3/nvD3.service");
var material_components_module_1 = require("../core/common/material-components.module");
var highlight_module_1 = require("../core/highlightjs/highlight.module");
var loading_overlay_component_1 = require("../core/loading-overlay/loading-overlay.component");
var scrollbar_module_1 = require("../core/scrollbar/scrollbar.module");
var activity_component_1 = require("../core/widgets/activity/activity.component");
var bar_chart_component_1 = require("../core/widgets/bar-chart/bar-chart.component");
var google_maps_widget_component_1 = require("../core/widgets/google-maps-widget/google-maps-widget.component");
var line_chart_component_1 = require("../core/widgets/line-chart/line-chart.component");
var message_widget_module_1 = require("../core/widgets/message-widget/message-widget.module");
var pie_chart_component_1 = require("../core/widgets/pie-chart/pie-chart.component");
var recent_sales_component_1 = require("../core/widgets/recent-sales/recent-sales.component");
var traffic_sources_component_1 = require("../core/widgets/traffic-sources/traffic-sources.component");
var forgot_password_component_1 = require("./custom-pages/forgot-password/forgot-password.component");
var login_component_1 = require("./custom-pages/login/login.component");
var register_component_1 = require("./custom-pages/register/register.component");
var dashboard_component_1 = require("./dashboard/dashboard.component");
var drag_and_drop_component_1 = require("./drag-and-drop/drag-and-drop.component");
var editor_component_1 = require("./editor/editor.component");
var form_elements_component_1 = require("./forms/form-elements/form-elements.component");
var form_wizard_component_1 = require("./forms/form-wizard/form-wizard.component");
var icons_component_1 = require("./icons/icons.component");
var level5_component_1 = require("./levels/level5/level5.component");
var google_maps_component_1 = require("./maps/google-maps/google-maps.component");
var DemoModule = /** @class */ (function () {
    function DemoModule() {
    }
    DemoModule = __decorate([
        core_2.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                forms_1.FormsModule,
                http_1.HttpClientModule,
                app_routing_1.RoutingModule,
                material_components_module_1.MaterialModule,
                flex_layout_1.FlexLayoutModule,
                core_1.AgmCoreModule.forRoot({
                    apiKey: environment_1.environment.googleApi
                }),
                ngx_quill_1.QuillModule,
                highlight_module_1.HighlightModule,
                angular_sortablejs_1.SortablejsModule,
                scrollbar_module_1.ScrollbarModule,
                message_widget_module_1.MessageWidgetModule
            ],
            declarations: [
                form_elements_component_1.FormElementsComponent,
                form_wizard_component_1.FormWizardComponent,
                google_maps_component_1.GoogleMapsComponent,
                nvD3_component_1.nvD3,
                icons_component_1.IconsComponent,
                level5_component_1.Level5Component,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                forgot_password_component_1.ForgotPasswordComponent,
                editor_component_1.EditorComponent,
                dashboard_component_1.DashboardComponent,
                bar_chart_component_1.BarChartComponent,
                line_chart_component_1.LineChartComponent,
                recent_sales_component_1.RecentSalesComponent,
                pie_chart_component_1.PieChartComponent,
                google_maps_widget_component_1.GoogleMapsWidgetComponent,
                activity_component_1.ActivityComponent,
                traffic_sources_component_1.TrafficSourcesComponent,
                loading_overlay_component_1.LoadingOverlayComponent,
                drag_and_drop_component_1.DragAndDropComponent,
            ],
            providers: [
                nvD3_service_1.D3ChartService
            ]
        })
    ], DemoModule);
    return DemoModule;
}());
exports.DemoModule = DemoModule;
//# sourceMappingURL=demo.module.js.map