"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/takeUntil");
var ReplaySubject_1 = require("rxjs/ReplaySubject");
var route_animation_1 = require("../../../core/common/route.animation");
var list_database_1 = require("../../../core/list/list-database");
var list_datasource_1 = require("../../../core/list/list-datasource");
var all_in_one_table_demo_1 = require("./all-in-one-table.demo");
var customer_create_update_component_1 = require("./customer-create-update/customer-create-update.component");
var customer_model_1 = require("./customer-create-update/customer.model");
var AllInOneTableComponent = /** @class */ (function () {
    function AllInOneTableComponent(dialog) {
        this.dialog = dialog;
        this.subject$ = new ReplaySubject_1.ReplaySubject(1);
        this.columns = [
            { name: 'Checkbox', property: 'checkbox', visible: false },
            { name: 'Image', property: 'image', visible: true },
            { name: 'Name', property: 'name', visible: true, isModelProperty: true },
            { name: 'First Name', property: 'firstName', visible: false, isModelProperty: true },
            { name: 'Last Name', property: 'lastName', visible: false, isModelProperty: true },
            { name: 'Street', property: 'street', visible: true, isModelProperty: true },
            { name: 'Zipcode', property: 'zipcode', visible: true, isModelProperty: true },
            { name: 'City', property: 'city', visible: true, isModelProperty: true },
            { name: 'Phone', property: 'phoneNumber', visible: true, isModelProperty: true },
            { name: 'Actions', property: 'actions', visible: true },
        ];
        this.pageSize = 10;
    }
    Object.defineProperty(AllInOneTableComponent.prototype, "visibleColumns", {
        get: function () {
            return this.columns.filter(function (column) { return column.visible; }).map(function (column) { return column.property; });
        },
        enumerable: true,
        configurable: true
    });
    AllInOneTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.customers = all_in_one_table_demo_1.ALL_IN_ONE_TABLE_DEMO_DATA.map(function (customer) { return new customer_model_1.Customer(customer); });
        this.subject$.next(this.customers);
        this.data$ = this.subject$.asObservable();
        this.database = new list_database_1.ListDatabase();
        this.data$
            .filter(Boolean)
            .subscribe(function (customers) {
            _this.customers = customers;
            _this.database.dataChange.next(customers);
            _this.resultsLength = customers.length;
        });
        this.dataSource = new list_datasource_1.ListDataSource(this.database, this.sort, this.paginator, this.columns);
    };
    AllInOneTableComponent.prototype.createCustomer = function () {
        var _this = this;
        this.dialog.open(customer_create_update_component_1.CustomerCreateUpdateComponent).afterClosed().subscribe(function (customer) {
            if (customer) {
                _this.customers.unshift(new customer_model_1.Customer(customer));
                _this.subject$.next(_this.customers);
            }
        });
    };
    AllInOneTableComponent.prototype.updateCustomer = function (customer) {
        var _this = this;
        this.dialog.open(customer_create_update_component_1.CustomerCreateUpdateComponent, {
            data: customer
        }).afterClosed().subscribe(function (customer) {
            if (customer) {
                var index = _this.customers.findIndex(function (existingCustomer) { return existingCustomer.id === customer.id; });
                _this.customers[index] = new customer_model_1.Customer(customer);
                _this.subject$.next(_this.customers);
            }
        });
    };
    AllInOneTableComponent.prototype.deleteCustomer = function (customer) {
        this.customers.splice(this.customers.findIndex(function (existingCustomer) { return existingCustomer.id === customer.id; }), 1);
        this.subject$.next(this.customers);
    };
    AllInOneTableComponent.prototype.onFilterChange = function (value) {
        if (!this.dataSource) {
            return;
        }
        this.dataSource.filter = value;
    };
    AllInOneTableComponent.prototype.ngOnDestroy = function () {
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], AllInOneTableComponent.prototype, "columns", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], AllInOneTableComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], AllInOneTableComponent.prototype, "sort", void 0);
    AllInOneTableComponent = __decorate([
        core_1.Component({
            selector: 'vr-all-in-one-table',
            templateUrl: './all-in-one-table.component.html',
            styleUrls: ['./all-in-one-table.component.scss'],
            animations: [route_animation_1.routeAnimation],
            host: { '[@routeAnimation]': 'true' }
        }),
        __metadata("design:paramtypes", [material_1.MatDialog])
    ], AllInOneTableComponent);
    return AllInOneTableComponent;
}());
exports.AllInOneTableComponent = AllInOneTableComponent;
//# sourceMappingURL=all-in-one-table.component.js.map