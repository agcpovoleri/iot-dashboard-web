"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var material_components_module_1 = require("../../../core/common/material-components.module");
var list_module_1 = require("../../../core/list/list.module");
var all_in_one_table_routing_module_1 = require("./all-in-one-table-routing.module");
var all_in_one_table_component_1 = require("./all-in-one-table.component");
var customer_create_update_module_1 = require("./customer-create-update/customer-create-update.module");
var AllInOneTableModule = /** @class */ (function () {
    function AllInOneTableModule() {
    }
    AllInOneTableModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                all_in_one_table_routing_module_1.AllInOneTableRoutingModule,
                forms_1.FormsModule,
                material_components_module_1.MaterialModule,
                // Core
                list_module_1.ListModule,
                customer_create_update_module_1.CustomerCreateUpdateModule
            ],
            declarations: [all_in_one_table_component_1.AllInOneTableComponent],
            exports: [all_in_one_table_component_1.AllInOneTableComponent]
        })
    ], AllInOneTableModule);
    return AllInOneTableModule;
}());
exports.AllInOneTableModule = AllInOneTableModule;
//# sourceMappingURL=all-in-one-table.module.js.map