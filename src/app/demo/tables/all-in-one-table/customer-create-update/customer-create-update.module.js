"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var flex_layout_1 = require("@angular/flex-layout");
var forms_1 = require("@angular/forms");
var material_1 = require("@angular/material");
var customer_create_update_component_1 = require("./customer-create-update.component");
var CustomerCreateUpdateModule = /** @class */ (function () {
    function CustomerCreateUpdateModule() {
    }
    CustomerCreateUpdateModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                flex_layout_1.FlexLayoutModule,
                material_1.MatDialogModule,
                material_1.MatInputModule,
                material_1.MatButtonModule,
                material_1.MatIconModule,
                material_1.MatRadioModule,
                material_1.MatSelectModule
            ],
            declarations: [customer_create_update_component_1.CustomerCreateUpdateComponent],
            entryComponents: [customer_create_update_component_1.CustomerCreateUpdateComponent],
            exports: [customer_create_update_component_1.CustomerCreateUpdateComponent]
        })
    ], CustomerCreateUpdateModule);
    return CustomerCreateUpdateModule;
}());
exports.CustomerCreateUpdateModule = CustomerCreateUpdateModule;
//# sourceMappingURL=customer-create-update.module.js.map