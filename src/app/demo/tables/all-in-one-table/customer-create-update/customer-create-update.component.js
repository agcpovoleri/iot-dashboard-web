"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var material_1 = require("@angular/material");
var CustomerCreateUpdateComponent = /** @class */ (function () {
    function CustomerCreateUpdateComponent(defaults, dialogRef, fb) {
        this.defaults = defaults;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.mode = 'create';
    }
    CustomerCreateUpdateComponent_1 = CustomerCreateUpdateComponent;
    CustomerCreateUpdateComponent.prototype.ngOnInit = function () {
        if (this.defaults) {
            this.mode = 'update';
        }
        else {
            this.defaults = {};
        }
        this.form = this.fb.group({
            id: [CustomerCreateUpdateComponent_1.id++],
            firstName: [this.defaults.firstName || '',],
            lastName: [this.defaults.lastName || ''],
            street: this.defaults.street || '',
            city: this.defaults.city || '',
            zipcode: this.defaults.zipcode || '',
            phoneNumber: this.defaults.phoneNumber || '',
        });
    };
    CustomerCreateUpdateComponent.prototype.save = function () {
        if (this.mode === 'create') {
            this.createCustomer();
        }
        else if (this.mode === 'update') {
            this.updateCustomer();
        }
    };
    CustomerCreateUpdateComponent.prototype.createCustomer = function () {
        var customer = this.form.value;
        this.dialogRef.close(customer);
    };
    CustomerCreateUpdateComponent.prototype.updateCustomer = function () {
        var customer = this.form.value;
        customer.id = this.defaults.id;
        this.dialogRef.close(customer);
    };
    CustomerCreateUpdateComponent.prototype.isCreateMode = function () {
        return this.mode === 'create';
    };
    CustomerCreateUpdateComponent.prototype.isUpdateMode = function () {
        return this.mode === 'update';
    };
    CustomerCreateUpdateComponent.id = 100;
    CustomerCreateUpdateComponent = CustomerCreateUpdateComponent_1 = __decorate([
        core_1.Component({
            selector: 'vr-customer-create-update',
            templateUrl: './customer-create-update.component.html',
            styleUrls: ['./customer-create-update.component.scss']
        }),
        __param(0, core_1.Inject(material_1.MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [Object, material_1.MatDialogRef,
            forms_1.FormBuilder])
    ], CustomerCreateUpdateComponent);
    return CustomerCreateUpdateComponent;
    var CustomerCreateUpdateComponent_1;
}());
exports.CustomerCreateUpdateComponent = CustomerCreateUpdateComponent;
//# sourceMappingURL=customer-create-update.component.js.map