"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Customer = /** @class */ (function () {
    function Customer(customer) {
        this.id = customer.id;
        this.firstName = customer.firstName;
        this.lastName = customer.lastName;
        this.street = customer.street;
        this.zipcode = customer.zipcode;
        this.city = customer.city;
        this.phoneNumber = customer.phoneNumber;
        this.mail = customer.mail;
    }
    Object.defineProperty(Customer.prototype, "name", {
        get: function () {
            var name = '';
            if (this.firstName && this.lastName) {
                name = this.firstName + ' ' + this.lastName;
            }
            else if (this.firstName) {
                name = this.firstName;
            }
            else if (this.lastName) {
                name = this.lastName;
            }
            return name;
        },
        set: function (value) {
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Customer.prototype, "address", {
        get: function () {
            return this.street + ", " + this.zipcode + " " + this.city;
        },
        set: function (value) {
        },
        enumerable: true,
        configurable: true
    });
    return Customer;
}());
exports.Customer = Customer;
//# sourceMappingURL=customer.model.js.map