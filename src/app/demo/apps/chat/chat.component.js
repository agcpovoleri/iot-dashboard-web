"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var sortBy_1 = require("lodash-es/sortBy");
var moment = require("moment");
var route_animation_1 = require("../../../core/common/route.animation");
var chat_demo_1 = require("./chat.demo");
var ChatComponent = /** @class */ (function () {
    function ChatComponent() {
    }
    ChatComponent.prototype.ngOnInit = function () {
        this.chats = sortBy_1.default(chat_demo_1.chatDemoData, 'lastMessageTime').reverse();
        this.activeChat = this.chats[0];
    };
    ChatComponent.prototype.setActiveChat = function (chat) {
        this.activeChat = chat;
    };
    ChatComponent.prototype.send = function () {
        if (this.newMessage) {
            this.chats[0].messages.push({
                message: this.newMessage,
                when: moment(),
                who: 'me'
            });
            this.newMessage = '';
        }
    };
    ChatComponent.prototype.clearMessages = function (activeChat) {
        activeChat.messages.length = 0;
    };
    ChatComponent = __decorate([
        core_1.Component({
            selector: 'vr-chat',
            templateUrl: './chat.component.html',
            styleUrls: ['./chat.component.scss'],
            host: {
                "[@fadeInAnimation]": 'true'
            },
            animations: [route_animation_1.fadeInAnimation]
        }),
        __metadata("design:paramtypes", [])
    ], ChatComponent);
    return ChatComponent;
}());
exports.ChatComponent = ChatComponent;
//# sourceMappingURL=chat.component.js.map