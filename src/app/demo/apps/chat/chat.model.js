"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Chat = /** @class */ (function () {
    function Chat(model) {
        if (model === void 0) { model = null; }
        this.picture = model.picture;
        this.name = model.name;
        this.messages = model.messages;
    }
    return Chat;
}());
exports.Chat = Chat;
//# sourceMappingURL=chat.model.js.map