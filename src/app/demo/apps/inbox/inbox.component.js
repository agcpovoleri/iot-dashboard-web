"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var sortBy_1 = require("lodash-es/sortBy");
var route_animation_1 = require("../../../core/common/route.animation");
var inbox_compose_component_1 = require("./inbox-compose/inbox-compose.component");
var mail_service_1 = require("./mail.service");
var InboxComponent = /** @class */ (function () {
    function InboxComponent(mailService, renderer, composeDialog, snackBar, cd) {
        this.mailService = mailService;
        this.renderer = renderer;
        this.composeDialog = composeDialog;
        this.snackBar = snackBar;
        this.cd = cd;
        this.shownMails = [];
        this.allMails = [];
        this.selectedMails = [];
        this.clickListeners = [];
    }
    InboxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.mailSubscription = this.mailService.mails$.subscribe(function (mails) {
            _this.allMails = sortBy_1.default(mails, 'when').reverse();
        });
        this.setShownMailsByGroup('primary');
    };
    InboxComponent.prototype.ngAfterViewChecked = function () {
        this.createMailListClickListeners();
    };
    InboxComponent.prototype.openComposeDialog = function () {
        var _this = this;
        var dialogRef = this.composeDialog.open(inbox_compose_component_1.InboxComposeComponent);
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                _this.snackBar.open(result);
            }
        });
    };
    InboxComponent.prototype.createMailListClickListeners = function () {
        var _this = this;
        this.clickListeners.forEach(function (listener) {
            listener();
        });
        this.mailList.forEach(function (elem, index) {
            _this.clickListeners.push(_this.renderer.listen(elem.nativeElement, 'click', function (event) {
                if (event.target.className != 'mat-checkbox-inner-container' && event.target.className != 'mat-ripple-background') {
                    _this.showMailDetail(_this.shownMails[index]);
                }
            }));
        });
    };
    InboxComponent.prototype.resetTemporaries = function () {
        this.shownMailDetail = null;
        this.respondActive = false;
        this.cd.markForCheck();
    };
    InboxComponent.prototype.showMailDetail = function (mail) {
        this.shownMailDetail = mail;
        mail.read = true;
        this.cd.markForCheck();
    };
    InboxComponent.prototype.setShownMailsByGroup = function (group) {
        this.shownMails = this.allMails.filter(function (mail) {
            return (mail.group == group);
        });
        this.shownMailsTypeGroup = group;
        this.resetTemporaries();
        this.cd.markForCheck();
    };
    InboxComponent.prototype.setShownMailsByType = function (type) {
        this.shownMails = this.allMails.filter(function (mail) {
            return (mail.type == type);
        });
        this.shownMailsTypeGroup = type;
        this.resetTemporaries();
        this.cd.markForCheck();
    };
    InboxComponent.prototype.setShownMailsToStarred = function () {
        this.shownMails = this.allMails.filter(function (mail) {
            return (mail.starred == true);
        });
        this.shownMailsTypeGroup = 'starred';
        this.resetTemporaries();
        this.cd.markForCheck();
    };
    InboxComponent.prototype.toggleSelectAllThreads = function () {
        if (this.selectedMails && this.selectedMails.length > 0) {
            this.selectedMails = this.shownMails;
        }
        else {
            this.selectedMails = [];
        }
        this.cd.markForCheck();
    };
    InboxComponent.prototype.toggleStarred = function (mail) {
        mail.starred = !mail.starred;
        this.cd.markForCheck();
    };
    InboxComponent.prototype.isSelected = function (mail) {
        return this.selectedMails.includes(mail);
    };
    InboxComponent.prototype.unreadMailsCount = function (group) {
        var count = this.allMails.filter(function (mail) { return (mail.read == false && mail.group == group); }).length;
        var text = '';
        if (count > 0) {
            text = "(" + count + ")";
        }
        return text;
    };
    InboxComponent.prototype.setRespondActive = function (active) {
        this.respondActive = active;
        this.cd.markForCheck();
    };
    InboxComponent.prototype.ngOnDestroy = function () {
        this.mailSubscription.unsubscribe();
        this.clickListeners.forEach(function (listener) {
            listener();
        });
    };
    __decorate([
        core_1.ViewChildren('mailList'),
        __metadata("design:type", core_1.QueryList)
    ], InboxComponent.prototype, "mailList", void 0);
    InboxComponent = __decorate([
        core_1.Component({
            selector: 'vr-inbox',
            templateUrl: './inbox.component.html',
            styleUrls: ['./inbox.component.scss'],
            host: {
                "[@fadeInAnimation]": 'true'
            },
            animations: [route_animation_1.fadeInAnimation],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [mail_service_1.MailService,
            core_1.Renderer,
            material_1.MatDialog,
            material_1.MatSnackBar,
            core_1.ChangeDetectorRef])
    ], InboxComponent);
    return InboxComponent;
}());
exports.InboxComponent = InboxComponent;
//# sourceMappingURL=inbox.component.js.map