"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var moment = require("moment");
var eventDateFormat = 'DD.MM.YYYY, HH:mm';
var CalendarEditComponent = /** @class */ (function () {
    function CalendarEditComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    CalendarEditComponent.prototype.ngOnInit = function () {
        this.event = this.dialogRef.componentInstance.event;
        this.eventStart = moment(this.event.start).format(eventDateFormat);
        this.eventEnd = moment(this.event.end).format(eventDateFormat);
    };
    CalendarEditComponent.prototype.save = function () {
        if (this.eventStart) {
            this.event.start = moment(this.eventStart, eventDateFormat).toDate();
        }
        if (this.eventEnd) {
            this.event.end = moment(this.eventEnd, eventDateFormat).toDate();
        }
        this.dialogRef.close(this.event);
    };
    CalendarEditComponent = __decorate([
        core_1.Component({
            selector: 'vr-calendar-edit',
            templateUrl: './calendar-edit.component.html',
            styleUrls: ['./calendar-edit.component.scss']
        }),
        __metadata("design:paramtypes", [material_1.MatDialogRef])
    ], CalendarEditComponent);
    return CalendarEditComponent;
}());
exports.CalendarEditComponent = CalendarEditComponent;
//# sourceMappingURL=calendar-edit.component.js.map