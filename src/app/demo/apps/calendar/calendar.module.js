"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var angular_calendar_1 = require("angular-calendar");
var material_components_module_1 = require("../../../core/common/material-components.module");
var calendar_edit_component_1 = require("./calendar-edit/calendar-edit.component");
var calendar_routing_module_1 = require("./calendar-routing.module");
var calendar_component_1 = require("./calendar.component");
var CalendarAppModule = /** @class */ (function () {
    function CalendarAppModule() {
    }
    CalendarAppModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                calendar_routing_module_1.CalendarRoutingModule,
                forms_1.FormsModule,
                material_components_module_1.MaterialModule,
                angular_calendar_1.CalendarModule.forRoot()
            ],
            declarations: [calendar_component_1.CalendarComponent, calendar_edit_component_1.CalendarEditComponent],
            entryComponents: [calendar_edit_component_1.CalendarEditComponent]
        })
    ], CalendarAppModule);
    return CalendarAppModule;
}());
exports.CalendarAppModule = CalendarAppModule;
//# sourceMappingURL=calendar.module.js.map