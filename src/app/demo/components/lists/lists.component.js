"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var ListsComponent = /** @class */ (function () {
    function ListsComponent() {
        this.listHTML = escape_1.default("<mat-list class=\"list mat-elevation-z1\">\n  <h3 mat-subheader>Contacts</h3>\n  <mat-list-item>\n    <img mat-list-avatar src=\"assets/img/avatars/1.png\">\n    <h3 matLine>John</h3>\n    <p matLine>\n      <span>Brunch?</span>\n      <span class=\"subline\">-- Did you want to go on Sunday? I was thinking</span>\n    </p>\n  </mat-list-item>\n  <mat-list-item>\n    <img mat-list-avatar src=\"assets/img/avatars/2.png\">\n    <h3 matLine>Peter</h3>\n    <p matLine>\n      <span>Summer BBQ</span>\n      <span class=\"subline\">-- Wish I could come, but I have some special</span>\n    </p>\n  </mat-list-item>\n  <mat-list-item>\n    <img mat-list-avatar src=\"assets/img/avatars/3.png\">\n    <h3 matLine>Nancy</h3>\n    <p matLine>\n      <span>Oui oui</span>\n      <span class=\"subline\">-- Have you booked the Paris trip?</span>\n    </p>\n  </mat-list-item>\n  <mat-divider></mat-divider>\n  <h3 mat-subheader>Other</h3>\n  <mat-list-item>\n    <img mat-list-avatar src=\"assets/img/avatars/4.png\">\n    <h3 matLine>Frank</h3>\n    <p matLine>\n      <span>Pretty decent!</span>\n      <span class=\"subline\">-- You look pretty... decent!</span>\n    </p>\n  </mat-list-item>\n  <mat-list-item>\n    <img mat-list-avatar src=\"assets/img/avatars/5.png\">\n    <h3 matLine>Donald</h3>\n    <p matLine>\n      <span>That's great!</span>\n      <span class=\"subline\">-- Great work mate!</span>\n    </p>\n  </mat-list-item>\n</mat-list>");
    }
    ListsComponent.prototype.ngOnInit = function () {
    };
    ListsComponent = __decorate([
        core_1.Component({
            selector: 'vr-lists',
            templateUrl: './lists.component.html',
            styleUrls: ['./lists.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [
                route_animation_1.routeAnimation
            ]
        }),
        __metadata("design:paramtypes", [])
    ], ListsComponent);
    return ListsComponent;
}());
exports.ListsComponent = ListsComponent;
//# sourceMappingURL=lists.component.js.map