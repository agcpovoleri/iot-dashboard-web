"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var DialogsComponent = /** @class */ (function () {
    function DialogsComponent(dialog) {
        this.dialog = dialog;
        this.dialogHTML = escape_1.default("<button mat-raised-button type=\"button\" (click)=\"openDialog()\" color=\"primary\">Open Dialog</button>\n<p *ngIf=\"result\">You chose: {{ result }}</p>\n");
    }
    DialogsComponent.prototype.openDialog = function () {
        var _this = this;
        this.dialog.open(DemoDialogComponent, {
            disableClose: false
        }).afterClosed().subscribe(function (result) {
            _this.result = result;
        });
    };
    DialogsComponent = __decorate([
        core_1.Component({
            selector: 'vr-dialogs',
            templateUrl: './dialogs.component.html',
            styleUrls: ['./dialogs.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [route_animation_1.routeAnimation]
        }),
        __metadata("design:paramtypes", [material_1.MatDialog])
    ], DialogsComponent);
    return DialogsComponent;
}());
exports.DialogsComponent = DialogsComponent;
var DemoDialogComponent = /** @class */ (function () {
    function DemoDialogComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    DemoDialogComponent.prototype.close = function (answer) {
        this.dialogRef.close(answer);
    };
    DemoDialogComponent = __decorate([
        core_1.Component({
            selector: 'vr-demo-dialog',
            template: "\n    <div mat-dialog-title fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n      <div>Question</div>\n      <button type=\"button\" mat-icon-button (click)=\"close('No answer')\" tabindex=\"-1\">\n        <mat-icon>close</mat-icon>\n      </button>\n    </div>\n\n    <mat-dialog-content>\n      <p>Do you like Pizza?</p>\n    </mat-dialog-content>\n\n\n    <mat-dialog-actions align=\"end\">\n      <button mat-button (click)=\"close('No')\">No</button>\n      <button mat-button color=\"primary\" (click)=\"close('Yes')\">Yes</button>\n  </mat-dialog-actions>\n  "
        }),
        __metadata("design:paramtypes", [material_1.MatDialogRef])
    ], DemoDialogComponent);
    return DemoDialogComponent;
}());
exports.DemoDialogComponent = DemoDialogComponent;
//# sourceMappingURL=dialogs.component.js.map