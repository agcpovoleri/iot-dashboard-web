"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var SnackBarComponent = /** @class */ (function () {
    function SnackBarComponent(snackBar) {
        this.snackBar = snackBar;
        this.snackbarHTML = escape_1.default("<button mat-raised-button (click)=\"openSnackbar()\">TRIGGER SNACKBAR</button>");
    }
    SnackBarComponent.prototype.ngOnInit = function () {
    };
    SnackBarComponent.prototype.openSnackbar = function () {
        this.snackBar.open('I\'m a notification!', 'CLOSE', {
            duration: 3000
        });
    };
    SnackBarComponent = __decorate([
        core_1.Component({
            selector: 'vr-snack-bar',
            templateUrl: './snack-bar.component.html',
            styleUrls: ['./snack-bar.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [route_animation_1.routeAnimation]
        }),
        __metadata("design:paramtypes", [material_1.MatSnackBar])
    ], SnackBarComponent);
    return SnackBarComponent;
}());
exports.SnackBarComponent = SnackBarComponent;
//# sourceMappingURL=snack-bar.component.js.map