"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var InputComponent = /** @class */ (function () {
    function InputComponent(cd) {
        this.cd = cd;
        this.inputHTML = escape_1.default("<mat-form-field>\n  <button *ngIf=\"!visible\" type=\"button\" mat-icon-button matPrefix (click)=\"show()\">\n    <mat-icon matPrefix>lock</mat-icon>\n  </button>\n  <button *ngIf=\"visible\" type=\"button\" mat-icon-button matPrefix (click)=\"hide()\">\n    <mat-icon matPrefix>lock_open</mat-icon>\n  </button>\n  <mat-placeholder>Password</mat-placeholder>\n  <input matInput [type]=\"inputType\">\n  <button *ngIf=\"!visible\" type=\"button\" mat-icon-button matSuffix (click)=\"show()\">\n    <mat-icon>visibility</mat-icon>\n  </button>\n  <button *ngIf=\"visible\" type=\"button\" mat-icon-button matSuffix (click)=\"hide()\">\n    <mat-icon>visibility_off</mat-icon>\n  </button>\n</mat-form-field>");
        this.inputType = 'password';
        this.visible = false;
    }
    InputComponent.prototype.ngOnInit = function () {
    };
    InputComponent.prototype.show = function () {
        this.inputType = 'text';
        this.visible = true;
        this.cd.markForCheck();
    };
    InputComponent.prototype.hide = function () {
        this.inputType = 'password';
        this.visible = false;
        this.cd.markForCheck();
    };
    InputComponent = __decorate([
        core_1.Component({
            selector: 'vr-input',
            templateUrl: './input.component.html',
            styleUrls: ['./input.component.scss']
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef])
    ], InputComponent);
    return InputComponent;
}());
exports.InputComponent = InputComponent;
//# sourceMappingURL=input.component.js.map