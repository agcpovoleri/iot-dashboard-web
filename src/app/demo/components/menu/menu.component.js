"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        this.menuHTML = escape_1.default("<button mat-icon-button [mdMenuTriggerFor]=\"menu\">\n  <mat-icon>more_vert</mat-icon>\n</button>\n\n<mat-menu #menu=\"mdMenu\">\n  <button mat-menu-item>\n    <mat-icon> dialpad </mat-icon>\n    <span> Redial </span>\n  </button>\n  <button mat-menu-item disabled>\n    <mat-icon> voicemail </mat-icon>\n    <span> Check voicemail </span>\n  </button>\n  <button mat-menu-item>\n    <mat-icon> notifications_off </mat-icon>\n    <span> Disable alerts </span>\n  </button>\n</mat-menu>");
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    MenuComponent = __decorate([
        core_1.Component({
            selector: 'vr-menu',
            templateUrl: './menu.component.html',
            styleUrls: ['./menu.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [route_animation_1.routeAnimation]
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());
exports.MenuComponent = MenuComponent;
//# sourceMappingURL=menu.component.js.map