"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var CardsComponent = /** @class */ (function () {
    function CardsComponent() {
        this.cardHTML = escape_1.default("<mat-card>\n  <mat-card-header>\n    <img mat-card-avatar src=\"assets/img/avatars/3.png\">\n    <mat-card-subtitle>\n      Yesterday\n    </mat-card-subtitle>\n    <mat-card-title>Gerald Morris</mat-card-title>\n  </mat-card-header>\n  <img mat-card-image src=\"assets/img/backgrounds/1.jpg\">\n  <mat-card-content>\n    <p>Piqued favour stairs it enable exeter as seeing. Remainder met improving but engrossed sincerity age. Better but length gay denied abroad are. Attachment astonished to on appearance imprudence so collecting in excellence. Tiled way blind lived whose new. The for fully had she there leave merit enjoy forth. </p>\n  </mat-card-content>\n  <mat-divider></mat-divider>\n  <mat-card-actions>\n    <div fxLayout=\"row\">\n      <button mat-icon-button>\n        <mat-icon>share</mat-icon>\n      </button>\n      <button mat-icon-button>\n        <mat-icon>favorite</mat-icon>\n      </button>\n      <span fxFlex></span>\n      <button mat-button>\n        More Info\n      </button>\n      <button mat-button>\n        Save as\n      </button>\n    </div>\n  </mat-card-actions>\n</mat-card>");
    }
    CardsComponent.prototype.ngOnInit = function () {
    };
    CardsComponent = __decorate([
        core_1.Component({
            selector: 'vr-cards',
            templateUrl: './cards.component.html',
            styleUrls: ['./cards.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [route_animation_1.routeAnimation]
        }),
        __metadata("design:paramtypes", [])
    ], CardsComponent);
    return CardsComponent;
}());
exports.CardsComponent = CardsComponent;
//# sourceMappingURL=cards.component.js.map