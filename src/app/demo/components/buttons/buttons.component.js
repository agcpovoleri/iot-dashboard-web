"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var ButtonsComponent = /** @class */ (function () {
    function ButtonsComponent() {
        this.flatButtonsHTML = escape_1.default("<button mat-button>Button</button>\n<button mat-button color=\"primary\">Primary</button>\n<button mat-button color=\"accent\">Accent</button>\n<button mat-button color=\"warn\">Warn</button>\n<button mat-button disabled=\"true\">Disabled</button>");
        this.raisedButtonsHTML = escape_1.default("<button mat-raised-button>Button</button>\n<button mat-raised-button color=\"primary\">Primary</button>\n<button mat-raised-button color=\"accent\">Accent</button>\n<button mat-raised-button color=\"warn\">Warn</button>\n<button mat-raised-button disabled=\"true\">Disabled</button>");
        this.fabHTML = escape_1.default("<button mat-fab color=\"primary\"><mat-icon>grade</mat-icon></button>\n<button mat-fab color=\"accent\"><mat-icon>favorite</mat-icon></button>\n<button mat-fab color=\"warn\"><mat-icon>build</mat-icon></button>\n<button mat-fab disabled=\"true\"><mat-icon>lock</mat-icon></button>\n<button mat-mini-fab color=\"primary\"><mat-icon>favorite</mat-icon></button>\n<button mat-mini-fab color=\"accent\"><mat-icon>thumb_up</mat-icon></button>\n<button mat-mini-fab color=\"warn\"><mat-icon>build</mat-icon></button>\n<button mat-mini-fab disabled=\"true\"><mat-icon>lock</mat-icon></button>");
        this.buttonToggleHTML = escape_1.default("<mat-button-toggle-group>\n  <mat-button-toggle value=\"left\"><mat-icon>format_align_left</mat-icon></mat-button-toggle>\n  <mat-button-toggle value=\"center\"><mat-icon>format_align_center</mat-icon></mat-button-toggle>\n  <mat-button-toggle value=\"right\"><mat-icon>format_align_right</mat-icon></mat-button-toggle>\n  <mat-button-toggle value=\"justify\"><mat-icon>format_align_justify</mat-icon></mat-button-toggle>\n</mat-button-toggle-group>\n<mat-button-toggle-group multiple>\n  <mat-button-toggle>Flour</mat-button-toggle>\n  <mat-button-toggle>Eggs</mat-button-toggle>\n  <mat-button-toggle>Sugar</mat-button-toggle>\n  <mat-button-toggle>Milk</mat-button-toggle>\n</mat-button-toggle-group>");
        this.iconButtonHTML = escape_1.default("<button mat-icon-button><mat-icon>menu</mat-icon></button>\n<button mat-icon-button color=\"primary\"><mat-icon>grade</mat-icon></button>\n<button mat-icon-button color=\"accent\"><mat-icon>favorite</mat-icon></button>\n<button mat-icon-button color=\"warn\"><mat-icon>build</mat-icon></button>\n<button mat-icon-button disabled=\"true\"><mat-icon>lock</mat-icon></button>");
    }
    ButtonsComponent.prototype.ngOnInit = function () {
    };
    ButtonsComponent = __decorate([
        core_1.Component({
            selector: 'vr-buttons',
            templateUrl: './buttons.component.html',
            styleUrls: ['./buttons.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [route_animation_1.routeAnimation]
        }),
        __metadata("design:paramtypes", [])
    ], ButtonsComponent);
    return ButtonsComponent;
}());
exports.ButtonsComponent = ButtonsComponent;
//# sourceMappingURL=buttons.component.js.map