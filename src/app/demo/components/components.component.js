"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var autocomplete_component_1 = require("./autocomplete/autocomplete.component");
var buttons_component_1 = require("./buttons/buttons.component");
var cards_component_1 = require("./cards/cards.component");
var checkbox_component_1 = require("./checkbox/checkbox.component");
var dialogs_component_1 = require("./dialogs/dialogs.component");
var grid_list_component_1 = require("./grid-list/grid-list.component");
var input_component_1 = require("./input/input.component");
var lists_component_1 = require("./lists/lists.component");
var menu_component_1 = require("./menu/menu.component");
var progress_spinner_component_1 = require("./progress-spinner/progress-spinner.component");
var progress_component_1 = require("./progress/progress.component");
var radio_component_1 = require("./radio/radio.component");
var slide_toggle_component_1 = require("./slide-toggle/slide-toggle.component");
var slider_component_1 = require("./slider/slider.component");
var snack_bar_component_1 = require("./snack-bar/snack-bar.component");
var tooltip_component_1 = require("./tooltip/tooltip.component");
var ComponentsComponent = /** @class */ (function () {
    function ComponentsComponent() {
    }
    ComponentsComponent.prototype.ngOnInit = function () {
    };
    ComponentsComponent.prototype.scrollTo = function (elem) {
        this[elem].nativeElement.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
            inline: 'nearest'
        });
    };
    __decorate([
        core_1.ViewChild(autocomplete_component_1.AutocompleteComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "autocomplete", void 0);
    __decorate([
        core_1.ViewChild(buttons_component_1.ButtonsComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "buttons", void 0);
    __decorate([
        core_1.ViewChild(cards_component_1.CardsComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "cards", void 0);
    __decorate([
        core_1.ViewChild(checkbox_component_1.CheckboxComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "checkbox", void 0);
    __decorate([
        core_1.ViewChild(dialogs_component_1.DialogsComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "dialogs", void 0);
    __decorate([
        core_1.ViewChild(grid_list_component_1.GridListComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "gridList", void 0);
    __decorate([
        core_1.ViewChild(input_component_1.InputComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "input", void 0);
    __decorate([
        core_1.ViewChild(lists_component_1.ListsComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "lists", void 0);
    __decorate([
        core_1.ViewChild(menu_component_1.MenuComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "menu", void 0);
    __decorate([
        core_1.ViewChild(progress_component_1.ProgressComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "progress", void 0);
    __decorate([
        core_1.ViewChild(progress_spinner_component_1.ProgressSpinnerComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "progressSpinner", void 0);
    __decorate([
        core_1.ViewChild(radio_component_1.RadioComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "radio", void 0);
    __decorate([
        core_1.ViewChild(slider_component_1.SliderComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "slider", void 0);
    __decorate([
        core_1.ViewChild(slide_toggle_component_1.SlideToggleComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "slideToggle", void 0);
    __decorate([
        core_1.ViewChild(snack_bar_component_1.SnackBarComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "snackBar", void 0);
    __decorate([
        core_1.ViewChild(tooltip_component_1.TooltipComponent, { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], ComponentsComponent.prototype, "tooltip", void 0);
    ComponentsComponent = __decorate([
        core_1.Component({
            selector: 'vr-components',
            templateUrl: './components.component.html',
            styleUrls: ['./components.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsComponent);
    return ComponentsComponent;
}());
exports.ComponentsComponent = ComponentsComponent;
//# sourceMappingURL=components.component.js.map