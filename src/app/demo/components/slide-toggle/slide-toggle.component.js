"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var SlideToggleComponent = /** @class */ (function () {
    function SlideToggleComponent() {
        this.slideToggleHTML = escape_1.default("<mat-slide-toggle [checked]=\"true\"></mat-slide-toggle>\n<mat-slide-toggle color=\"primary\" [checked]=\"true\"></mat-slide-toggle>\n<mat-slide-toggle color=\"accent\" [checked]=\"true\"></mat-slide-toggle>\n<mat-slide-toggle color=\"warn\" [checked]=\"true\"></mat-slide-toggle>\n<mat-slide-toggle disabled></mat-slide-toggle>");
    }
    SlideToggleComponent.prototype.ngOnInit = function () {
    };
    SlideToggleComponent = __decorate([
        core_1.Component({
            selector: 'vr-slide-toggle',
            templateUrl: './slide-toggle.component.html',
            styleUrls: ['./slide-toggle.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], SlideToggleComponent);
    return SlideToggleComponent;
}());
exports.SlideToggleComponent = SlideToggleComponent;
//# sourceMappingURL=slide-toggle.component.js.map