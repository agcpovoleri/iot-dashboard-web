"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var ProgressComponent = /** @class */ (function () {
    function ProgressComponent() {
        this.progressHTML = escape_1.default("<mat-progress-bar mode=\"determinate\" [value]=\"40\"></mat-progress-bar>\n<mat-progress-bar mode=\"indeterminate\" color=\"primary\"></mat-progress-bar>\n<mat-progress-bar mode=\"buffer\" color=\"accent\"></mat-progress-bar>\n<mat-progress-bar mode=\"query\" color=\"warn\"></mat-progress-bar>");
    }
    ProgressComponent.prototype.ngOnInit = function () {
    };
    ProgressComponent = __decorate([
        core_1.Component({
            selector: 'vr-progress',
            templateUrl: './progress.component.html',
            styleUrls: ['./progress.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], ProgressComponent);
    return ProgressComponent;
}());
exports.ProgressComponent = ProgressComponent;
//# sourceMappingURL=progress.component.js.map