"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var route_animation_1 = require("../../../core/common/route.animation");
var GridListComponent = /** @class */ (function () {
    function GridListComponent() {
        this.tiles = [
            { text: 'One', cols: 3, rows: 1, color: 'lightblue' },
            { text: 'Two', cols: 1, rows: 2, color: 'lightgreen' },
            { text: 'Three', cols: 1, rows: 1, color: 'lightpink' },
            { text: 'Four', cols: 2, rows: 1, color: '#DDBDF1' },
        ];
        this.gridListHTML = escape_1.default("<mat-grid-list cols=\"4\" rowHeight=\"100px\">\n  <mat-grid-tile *ngFor=\"let tile of tiles\" [colspan]=\"tile.cols\" [rowspan]=\"tile.rows\"\n                  [style.background]=\"tile.color\">\n      {{tile.text}}\n  </mat-grid-tile>\n</mat-grid-list>");
    }
    GridListComponent.prototype.ngOnInit = function () {
    };
    GridListComponent = __decorate([
        core_1.Component({
            selector: 'vr-grid-list',
            templateUrl: './grid-list.component.html',
            styleUrls: ['./grid-list.component.scss'],
            host: {
                '[@routeAnimation]': 'true'
            },
            animations: [route_animation_1.routeAnimation]
        }),
        __metadata("design:paramtypes", [])
    ], GridListComponent);
    return GridListComponent;
}());
exports.GridListComponent = GridListComponent;
//# sourceMappingURL=grid-list.component.js.map