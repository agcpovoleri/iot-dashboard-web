"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var material_components_module_1 = require("../../core/common/material-components.module");
var highlight_module_1 = require("../../core/highlightjs/highlight.module");
var scrollbar_module_1 = require("../../core/scrollbar/scrollbar.module");
var autocomplete_component_1 = require("./autocomplete/autocomplete.component");
var buttons_component_1 = require("./buttons/buttons.component");
var cards_component_1 = require("./cards/cards.component");
var checkbox_component_1 = require("./checkbox/checkbox.component");
var components_routing_module_1 = require("./components-routing.module");
var components_component_1 = require("./components.component");
var dialogs_component_1 = require("./dialogs/dialogs.component");
var grid_list_component_1 = require("./grid-list/grid-list.component");
var input_component_1 = require("./input/input.component");
var lists_component_1 = require("./lists/lists.component");
var menu_component_1 = require("./menu/menu.component");
var progress_spinner_component_1 = require("./progress-spinner/progress-spinner.component");
var progress_component_1 = require("./progress/progress.component");
var radio_component_1 = require("./radio/radio.component");
var slide_toggle_component_1 = require("./slide-toggle/slide-toggle.component");
var slider_component_1 = require("./slider/slider.component");
var snack_bar_component_1 = require("./snack-bar/snack-bar.component");
var tooltip_component_1 = require("./tooltip/tooltip.component");
var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                components_routing_module_1.ComponentsRoutingModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                material_components_module_1.MaterialModule,
                highlight_module_1.HighlightModule,
                scrollbar_module_1.ScrollbarModule
            ],
            declarations: [
                components_component_1.ComponentsComponent,
                autocomplete_component_1.AutocompleteComponent,
                buttons_component_1.ButtonsComponent,
                cards_component_1.CardsComponent,
                checkbox_component_1.CheckboxComponent,
                dialogs_component_1.DialogsComponent,
                dialogs_component_1.DemoDialogComponent,
                input_component_1.InputComponent,
                grid_list_component_1.GridListComponent,
                lists_component_1.ListsComponent,
                menu_component_1.MenuComponent,
                progress_component_1.ProgressComponent,
                progress_spinner_component_1.ProgressSpinnerComponent,
                radio_component_1.RadioComponent,
                slider_component_1.SliderComponent,
                snack_bar_component_1.SnackBarComponent,
                tooltip_component_1.TooltipComponent,
                slide_toggle_component_1.SlideToggleComponent,
            ],
            entryComponents: [dialogs_component_1.DemoDialogComponent]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());
exports.ComponentsModule = ComponentsModule;
//# sourceMappingURL=components.module.js.map