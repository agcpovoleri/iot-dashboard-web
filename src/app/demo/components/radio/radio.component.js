"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var RadioComponent = /** @class */ (function () {
    function RadioComponent() {
        this.radioHTML = escape_1.default("<mat-radio-group [(ngModel)]=\"favoriteSeason\">\n  <mat-radio-button class=\"radio\" *ngFor=\"let season of seasons\" [value]=\"season\">\n    {{ season }}\n  </mat-radio-button>\n</mat-radio-group>");
        this.seasons = [
            'Winter',
            'Spring',
            'Summer',
            'Autumn',
        ];
    }
    RadioComponent.prototype.ngOnInit = function () {
    };
    RadioComponent = __decorate([
        core_1.Component({
            selector: 'vr-radio',
            templateUrl: './radio.component.html',
            styleUrls: ['./radio.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], RadioComponent);
    return RadioComponent;
}());
exports.RadioComponent = RadioComponent;
//# sourceMappingURL=radio.component.js.map