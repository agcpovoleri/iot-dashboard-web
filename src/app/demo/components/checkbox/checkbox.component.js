"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var escape_1 = require("lodash-es/escape");
var CheckboxComponent = /** @class */ (function () {
    function CheckboxComponent() {
        this.checkboxHTML = escape_1.default("<mat-checkbox>Checkbox</mat-checkbox>\n<mat-checkbox color=\"primary\" [checked]=\"true\">Primary</mat-checkbox>\n<mat-checkbox color=\"accent\" [checked]=\"true\">Accent</mat-checkbox>\n<mat-checkbox color=\"warn\" [checked]=\"true\">Warn</mat-checkbox>\n<mat-checkbox disabled [checked]=\"true\">Disabled</mat-checkbox>\n<mat-checkbox [indeterminate]=\"true\">Indeterminate</mat-checkbox>");
    }
    CheckboxComponent.prototype.ngOnInit = function () {
    };
    CheckboxComponent = __decorate([
        core_1.Component({
            selector: 'vr-checkbox',
            templateUrl: './checkbox.component.html',
            styleUrls: ['./checkbox.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], CheckboxComponent);
    return CheckboxComponent;
}());
exports.CheckboxComponent = CheckboxComponent;
//# sourceMappingURL=checkbox.component.js.map