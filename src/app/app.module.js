"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/common/http");
var app_component_1 = require("./app.component");
var app_routing_1 = require("./app.routing");
var index_1 = require("./_guards/index");
var index_2 = require("./_helpers/index");
var index_3 = require("./_services/index");
var core_module_1 = require("./core/core.module");
var demo_module_1 = require("./demo/demo.module");
var common_1 = require("@angular/common");
var animations_1 = require("@angular/platform-browser/animations");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpClientModule,
                app_routing_1.RoutingModule,
                // Core Angular Module // Don't remove!
                common_1.CommonModule,
                animations_1.BrowserAnimationsModule,
                // Fury Core Modules
                core_module_1.CoreModule,
                app_routing_1.RoutingModule,
                demo_module_1.DemoModule,
            ],
            providers: [
                index_1.AuthGuard,
                index_3.AlertService,
                index_3.AuthenticationService,
                index_3.UserService,
                index_3.SensorService,
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: index_2.JwtInterceptor,
                    multi: true
                },
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map